# Teclead Ventures
## Recruiting Task

Spring Boot Service with REST API, developed as part of the recruiting process with Teclead Ventures. 

## Running the Application

### 1. Building the Application

`./mvnw clean install`

### 2. Building the Docker Container

`docker build -t simon/usertestservice .`

### 3. Starting the Container

`docker run -p 9000:9000 simon/usertestservice .`

or 8000:8000 if the profile is set to dev

## API

A REST API provides endpoints for all CRUD Operations of the User Entity,
and additionally an endpoint to fetch all users with a certain firstname.

### Documentation

The API-Documentation can be found at

HTML:
http://localhost:9000/swagger-ui/index.html#/

JSON:
http://localhost:9000/v3/api-docs

YAML:
http://localhost:9000/v3/api-docs.yaml

### Testing

The API can be tested with Postman, after importing the collection and environments. 

Those files can be found inside the /postman directory in the project's root folder.

## Domain

This user domain only consists of a single Entity also called User.

This User Entity consists of four fields:
* id
* firstname
* lastname
* email

### id

This field is used for database purposes and doesn't have to be included in requests when creating new users.
It uses an auto-generation strategy of simply incrementing the id with each created object.

### firstname

This field is validated as mandatory

### lastname

This field is validated as mandatory

### email

This field is validated as mandatory and additionally there is a very simple Regex check.

This Regex only checks for at least one character as the username 
and one non-whitespace character for the domain part, as well as
whether they are separated by an @-Symbol.

This allows for a good deal of invalid email addresses, 
but I decided not to overcomplicate things for this exercise.

Which email addresses are in fact valid differs between many providers, 
and pretty much none of them allow for every possibility 
according to the official email address specification. 
