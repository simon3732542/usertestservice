package com.example.usertestservice.user;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;


    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }


    @Override
    public User getUserById(Long userId) {
        Optional<User> user = userRepository.findById(userId);
        return user.orElseThrow(() -> new UserNotFoundException(userId));
    }


    @Override
    public List<User> findUsersByFirstname(String firstname) {
        return userRepository.findByFirstname(firstname);
    }


    @Override
    public User createUser(User user) {
        return userRepository.save(user);
    }


    @Override
    public User updateUser(Long userId, User user) {
        return userRepository.findById(userId).map(oldUser -> {
            // on purpose no null checks here so values can be deleted - the frontend should send all fields, not just the ones it wants updated
            oldUser.setFirstname(user.getFirstname());
            oldUser.setLastname(user.getLastname());
            oldUser.setEmail(user.getEmail());
            return userRepository.save(oldUser);
        }).orElseThrow(
                // we could also save a new user object, but this is a little cleaner
                () -> new UserNotFoundException(userId));
    }


    @Override
    public void deleteUser(Long userId) {
        userRepository.deleteById(userId);
    }
}
