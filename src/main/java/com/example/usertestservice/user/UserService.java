package com.example.usertestservice.user;

import java.util.List;

public interface UserService {

    List<User> findAll();
    User getUserById(Long userId);
    List<User> findUsersByFirstname(String firstname);
    User createUser(User user);
    User updateUser(Long userId, User user);
    void deleteUser(Long userId);
}
