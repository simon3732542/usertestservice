package com.example.usertestservice.user;

public class UserNotFoundException extends RuntimeException {

    public UserNotFoundException(Long userId) {
        super("Can't find User with Id: " + userId);
    }
}
