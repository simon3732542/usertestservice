package com.example.usertestservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UserTestServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(UserTestServiceApplication.class, args);
    }

}
