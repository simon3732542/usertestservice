package com.example.usertestservice.user;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.assertj.core.api.BDDAssertions.then;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
class UserServiceImplTest {

    private UserService userService;

    @Mock
    private UserRepository userRepository;

    @BeforeEach
    public void setUp() {
        userService = new UserServiceImpl(userRepository);
    }

    @Test
    public void findAllTest() {
        // given
        User user1 = new User(1L, "Susie", "Sommer", "susiesommer@gmail.com");
        User user2 = new User(1L, "Manfred", "Mustermann", "manfred.mustermann@outlook.de");
        given(userRepository.findAll()).willReturn(List.of(user1, user2));

        // when
        List<User> foundUsers = userService.findAll();

        // then
        then(foundUsers).containsExactlyInAnyOrder(user1, user2);
    }
}